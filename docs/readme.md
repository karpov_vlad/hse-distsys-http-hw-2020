# Документация на мой API

#### create
Создает пользователя с входными параметрами

Параметры: 
1. username - имя пользователя
2. real_name - настроящее имя пользователя

Возвращаемое значение (словарь):
1. Если пользователь создался
    1. Id пользователя
    2. Информация о пользователе
2. Если нет нужных параметров:
    1. Parameters 'username' and 'real_name' are required

#### get_list_of_users
Возвращает список пользователей

Возвращаемое значение:
1. Список элемент из 3 полей
    1. Id пользователя
    2. Имя о пользователя
    3. Настоящие имя пользователя


#### delete
Удаляет пользователя с входным Id

Параметры: 
1. user_id - Id пользователя

Возвращаемое значение:
1. Если пользователь удалился
    1. Id пользователя
    2. Информация о пользователе
2. Если пользователя не было
    1. User with user_id={user_id} not founded
3. Если нет нужных параметров:
    1. Parameter 'user_id' is required


#### update
Обновляет пользователя с входным Id

Параметры: 
1. user_id - Id пользователя
2. username - новое имя пользователя
3. real_name - новое настроящее имя пользователя

Возвращаемое значение:
1. Если пользователь обновился
    1. Id пользователя
    2. Информация о пользователе
2. Если пользователя не было
    1. User with user_id={user_id} not founded
3. Если нет нужных параметров:
    1. Parameters 'user_id', 'username' and 'real_name' are required
