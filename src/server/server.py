import os

from dataclasses import dataclass
from typing import List, Tuple, Dict, Optional
from flask import Flask, request


@dataclass
class User:
    username: str
    real_name: str

    def to_dict(self):
        return {'username': self.username, 'real_name': self.real_name}


class UsersStorage:
    def __init__(self):
        self._sequence_id = 0
        self.data: Dict[int, User] = dict()  # key - sequence_id, value - user

    def create_user(self, new_user: User) -> Tuple[int, User]:
        self.data[self._sequence_id] = new_user
        self._sequence_id += 1
        return self._sequence_id - 1, new_user

    def get_users_list(self) -> List[Tuple[int, str, str]]:
        return [(user_id, user.username, user.real_name) for user_id, user in self.data.items()]

    def delete_user_by_id(self, user_id: int) -> Optional[User]:
        """
        :returns: User if user is deleted successfully and None in other way
        """
        return self.data.pop(user_id, None)

    def modify_user_by_id(self, user_id: int, updated_user: User) -> Optional[User]:
        """
        :returns: User if user is updated successfully and None in other way
        """
        if user_id in self.data:
            self.data[user_id] = updated_user
            return updated_user

    
storage = UsersStorage()
app = Flask(__name__)


@app.route('/')
def hello_world():
    return 'Hello, World!'


@app.route('/create/', methods=['POST'])
def create():
    if (username := request.args.get('username')) and (real_name := request.args.get('real_name')):
        user_id, user = storage.create_user(User(username, real_name))
        return {'user_id': user_id, 'user_info': user.to_dict()}, 201
    return {'status': "Parameters 'username' and 'real_name' are required"}


@app.route('/get_list_of_users/', methods=['GET'])
def get_list_of_users():
    list_of_users = storage.get_users_list()
    return {'users_count': len(list_of_users), 'users': storage.get_users_list()}


@app.route('/delete/', methods=['DELETE'])
def delete():
    if user_id := request.args.get('user_id'):
        if deleted_user := storage.delete_user_by_id(int(user_id)):
            return {'user_id': user_id, 'user_info': deleted_user.to_dict()}, 202
        return {'status': f'User with user_id={user_id} not founded'}
    return {'status': "Parameter 'user_id' is required"}


@app.route('/update/', methods=['PUT'])
def update():
    if ((user_id := request.args.get('user_id')) and
            (username := request.args.get('username')) and
            (real_name := request.args.get('real_name'))):
        if modified_user := storage.modify_user_by_id(int(user_id), User(username, real_name)):
            return {'user_id': user_id, 'user_info': modified_user.to_dict()}
        return {'status': f'User with user_id={user_id} not founded'}
    return {'status': "Parameters 'user_id', 'username' and 'real_name' are required"}


app.run(host='0.0.0.0', port=int(os.environ.get('HSE_HTTP_FLASK_PORT', 80)))
