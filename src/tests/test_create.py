import requests

from config import URL


def test_create_user():
    user_info = {'username': 'username_0', 'real_name': 'real_name_0'}
    response = requests.post(
        URL + '/create/',
        params=user_info
    )
    assert response.json()['user_info'] == user_info
    assert response.status_code == 201
