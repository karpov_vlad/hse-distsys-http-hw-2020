import requests

from config import URL


def test_update_user():
    user_info = {'username': 'username_4', 'real_name': 'real_name_4'}
    response = requests.post(
        URL + '/create/',
        params=user_info
    )
    assert response.json()['user_info'] == user_info
    assert response.status_code == 201

    new_user_info = {'username': 'new_username_4', 'real_name': 'new_real_name_4'}
    user_id = int(response.json()['user_id'])
    response = requests.put(
        URL + '/update/',
        params={'user_id': user_id, **new_user_info}
    )
    assert response.json() == {'user_id': str(user_id), 'user_info': new_user_info}
    assert response.status_code == 200
