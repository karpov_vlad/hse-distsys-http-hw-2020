import requests

from config import URL


def test_get_list_of_users():
    response = requests.get(
        URL + '/get_list_of_users/'
    )
    for user_info in response.json()['users']:
        assert type(user_info[0]) == int
        assert type(user_info[1]) == str
        assert type(user_info[2]) == str
    assert response.status_code == 200
