import requests

from config import URL


def test_delete_user():
    user_info = {'username': 'username_3', 'real_name': 'real_name_3'}
    response = requests.post(
        URL + '/create/',
        params=user_info
    )
    assert response.json()['user_info'] == user_info
    assert response.status_code == 201

    user_id = int(response.json()['user_id'])
    response = requests.delete(
        URL + '/delete/',
        params={'user_id': user_id}
    )
    assert response.json() == {'user_id': str(user_id), 'user_info': user_info}
    assert response.status_code == 202
